package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Long idUser;

    @Column(name = "names")
    private String names;

    @Column(name = "lastnames")
    private String lastnames;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="strategic_plan_users", joinColumns = {@JoinColumn(name = "users_id_user")},
            inverseJoinColumns = {@JoinColumn(name = "strategic_plan_id_strategic_plan")})
    private List<StrategicPlan> strategicPlans;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="activity_users", joinColumns = {@JoinColumn(name = "users_id_user")},
            inverseJoinColumns = {@JoinColumn(name = "activity_id_activity")})
    private List<Activity> activities;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    public User(String names, String lastnames, String phone, String email, String password) {
        this.names = names;
        this.lastnames = lastnames;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public User() {
        this.setStrategicPlans(new ArrayList<>());
    }
}
