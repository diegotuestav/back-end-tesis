package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "indicator")
public class Indicator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_indicator")
    private Long idIndicator;

    @ManyToOne
    @JoinColumn(name="id_strategicPlan")
    private StrategicPlan strategicPlan;

    @ManyToOne
    @JoinColumn(name="id_indicatorType")
    private IndicatorType indicatorType;

    @OneToOne(mappedBy = "indicator")
    private Quiz quiz;

    @JoinColumn(name="goal")
    private Double goal;

    @JoinColumn(name="variable1")
    private Double variable1;

    @JoinColumn(name="variable2")
    private Double variable2;

    @JoinColumn(name="result")
    private Double result;


    public Indicator(IndicatorType indicatorType) {
        this.indicatorType = indicatorType;
    }

}