package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "alternative")
public class Alternative {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_alternative")
    private Long idAlternative;

    @ManyToOne
    @JoinColumn(name="id_question")
    private Question question;


    @Column(name = "alternative")
    private String alternative;

    @Column(name = "is_correct")
    private boolean isCorrect;
}