package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_question")
    private Long idQuestion;

    @ManyToOne
    @JoinColumn(name="id_quiz")
    private Quiz quiz;

    @OneToMany(mappedBy = "question")
    private List<Alternative> alternatives;

    @Column(name = "num_question")
    private Integer numQuestion;

    @Column(name = "statement")
    private String statement;

    @Column(name = "score")
    private Double score;
}
