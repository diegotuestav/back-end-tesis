package com.tesis20161998.tesis20161998.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@Entity
@Table(name = "sponsorship")
public class Sponsorship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sponsorship")
    private Long idSponsorship;

    @Column(name = "names")
    private String names;

    @Column(name = "lastnames")
    private String lastnames;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "charge")
    private String charge;

    @ManyToOne
    @JoinColumn(name="id_role")
    private Role role;

    @ManyToOne
    @JoinColumn(name="id_strategic_plan")
    private StrategicPlan strategicPlan;

}
