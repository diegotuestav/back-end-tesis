package com.tesis20161998.tesis20161998.models;

import lombok.Data;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

@Data
@Entity
@Table(name = "macroactivity")
public class MacroActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_macroactivity")
    private Long idMacroActivity;

    @ManyToOne
    @JoinColumn(name="id_macroactivityType")
    private MacroActivityType macroActivityType;

    @ManyToOne
    @JoinColumn(name="id_strategicPlan")
    private StrategicPlan strategicPlan;

    @OneToMany(mappedBy = "macroActivity")
    private List<Activity> activities;

    @Column(length = 10000, name = "blankSpace1")
    private String blankSpace1;

    @Column(length = 10000, name = "blankSpace2")
    private String blankSpace2;

    @Column(length = 10000, name = "blankSpace3")
    private String blankSpace3;

    @Column(length = 10000, name = "blankSpace4")
    private String blankSpace4;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name=" document1", columnDefinition="longblob", nullable=true)
    private byte[] document1;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name=" document2", columnDefinition="longblob", nullable=true)
    private byte[] document2;
}
