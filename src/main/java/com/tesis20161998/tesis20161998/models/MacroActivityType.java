package com.tesis20161998.tesis20161998.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "macroactivity_type")
public class MacroActivityType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_macroactivityType")
    private Long idMacroActivityType;

    @Column(name = "name_macroactivity_type")
    private String nameMacroActivity;

    @OneToMany(mappedBy = "macroActivityType")
    private List<MacroActivity> macroActivities;

    public MacroActivityType(String nameMacroActivity) {
        this.nameMacroActivity = nameMacroActivity;
    }
}
