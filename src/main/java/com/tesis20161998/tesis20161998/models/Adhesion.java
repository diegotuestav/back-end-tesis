package com.tesis20161998.tesis20161998.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "adhesion")
public class Adhesion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_adhesion")
    private Long idAdhesion;

    @Column(name = "adhesion")
    private String adhesion;

    @OneToMany(mappedBy = "adhesion")
    private List<Stakeholder> stakeholders;

    public Adhesion(String adhesion) {
        this.adhesion = adhesion;
    }
}
