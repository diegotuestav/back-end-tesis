package com.tesis20161998.tesis20161998.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "stakeholder")
public class Stakeholder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_stakeholder")
    private Long idStakeholder;

    @Column(name = "names")
    private String names;

    @Column(name = "lastnames")
    private String lastnames;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @ManyToOne
    @JoinColumn(name="id_profile")
    private Profile profile;

    @ManyToOne
    @JoinColumn(name="id_adhesion")
    private Adhesion adhesion;

    @ManyToOne
    @JoinColumn(name="id_collective")
    private Collective collective;

    @ManyToOne
    @JoinColumn(name="id_strategicPlan")
    private StrategicPlan strategicPlan;
}
