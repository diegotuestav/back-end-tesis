package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "quizResolved")
public class QuizResolved {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_quizresolved")
    private Long idQuizResolved;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name ="questions",columnDefinition = "text")
    private String questions;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name ="alternatives",columnDefinition = "text")
    private String alternatives;
}
