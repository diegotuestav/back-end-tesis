package com.tesis20161998.tesis20161998.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "collective")
public class Collective {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_collective")
    private Long idCollective;

    @Column(name = "collective")
    private String collective;

    @ManyToOne
    @JoinColumn(name="id_strategicPlan")
    private StrategicPlan strategicPlan;

    @OneToMany(mappedBy = "collective")
    private List<Stakeholder> stakeholders;
}