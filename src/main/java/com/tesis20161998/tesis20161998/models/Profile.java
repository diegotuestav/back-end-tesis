package com.tesis20161998.tesis20161998.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "profile")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_profile")
    private Long idProfile;

    @Column(name = "profile")
    private String profile;

    @OneToMany(mappedBy = "profile")
    private List<Stakeholder> stakeholders;

    public Profile(String profile) {
        this.profile = profile;
    }
}
