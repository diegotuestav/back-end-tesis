package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "indicatorType")
public class IndicatorType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_indicatorType")
    private Long idIndicatorType;

    @OneToMany(mappedBy = "indicatorType")
    private List<Indicator> indicators;

    @Column(name = "indicator")
    private String indicator;

    @Column(name = "description")
    private String description;

    @Column(name = "period")
    private String period;

    @Column(name = "formula")
    private String formula;

    @Column(name = "nameVariable1")
    private String nameVariable1;

    @Column(name = "variableDescription1")
    private String variableDescription1;

    @Column(name = "nameVariable2")
    private String nameVariable2;

    @Column(name = "variableDescription2")
    private String variableDescription2;

    @Column(name = "measureUnit")
    private String measureUnit;


    public IndicatorType(String indicator, String description,String nameVariable1, String variableDescription1,
                     String nameVariable2, String variableDescription2, String measureUnit, String formula) {
        this.indicator = indicator;
        this.description = description;
        this.nameVariable1 = nameVariable1;
        this.variableDescription1 = variableDescription1;
        this.nameVariable2 = nameVariable2;
        this.variableDescription2 = variableDescription2;
        this.measureUnit = measureUnit;
        this.formula = formula;
    }

    public IndicatorType(String indicator, String description,String period, String nameVariable1, String variableDescription1,
                     String nameVariable2, String variableDescription2, String measureUnit, String formula) {
        this.indicator = indicator;
        this.description = description;
        this.period = period;
        this.nameVariable1 = nameVariable1;
        this.variableDescription1 = variableDescription1;
        this.nameVariable2 = nameVariable2;
        this.variableDescription2 = variableDescription2;
        this.measureUnit = measureUnit;
        this.formula = formula;
    }

    public IndicatorType() {
        this.setIndicators(new ArrayList<>());
    }
}