package com.tesis20161998.tesis20161998.models;

import lombok.Data;

import javax.persistence.*;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "activity")
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_activity")
    private Long idActivity;

    @ManyToOne
    @JoinColumn(name="id_macroActivity")
    private MacroActivity macroActivity;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "notes")
    private String notes;

    @Column(name = "endDate")
    private Date endDate;

    @Column(name = "finished")
    private boolean finished;

    @Lob
    @Column(name = "document")
    private byte[] document;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "activities")
    private List<User> users;

    public Activity() {
        this.setUsers(new ArrayList<>());
    }
}
