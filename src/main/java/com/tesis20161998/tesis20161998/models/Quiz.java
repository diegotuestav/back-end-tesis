package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_quiz")
    private Long idQuiz;

    @OneToOne
    @JoinColumn(name = "id_indicator")
    private Indicator indicator;

    @OneToMany(mappedBy = "quiz")
    private List<Question> questions;

    @Column(name = "numResolved")
    private Double numResolved;

    @Column(name = "numPercent")
    private Double numPercent;

    @Column(name = "title")
    private String title;

    @Column(name = "introduction")
    private String introduction;
}
