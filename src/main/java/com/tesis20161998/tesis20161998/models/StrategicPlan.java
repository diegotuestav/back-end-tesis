package com.tesis20161998.tesis20161998.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "strategic_plan")
public class StrategicPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_strategic_plan")
    private Long idStrategicPlan; //Init

    @Column(name = "organization_name")
    private String organizationName; //Init

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "strategicPlans")
    private List<User> users; //Init

    @OneToMany(mappedBy = "strategicPlan")
    private List<Indicator> indicators; //Init

    @OneToMany(mappedBy = "strategicPlan")
    private List<Sponsorship> sponsors;

    @OneToMany(mappedBy = "strategicPlan")
    private List<Stakeholder> stakeholders;

    @OneToMany(mappedBy = "strategicPlan")
    private List<Collective> collectives;

    @OneToMany(mappedBy = "strategicPlan")
    private List<MacroActivity> macroActivities; //Init

    public StrategicPlan() {
        this.setUsers(new ArrayList<>());
    }
}
