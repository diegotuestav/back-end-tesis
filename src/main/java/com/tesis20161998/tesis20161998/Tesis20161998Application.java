package com.tesis20161998.tesis20161998;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tesis20161998Application {

	public static void main(String[] args) {
		SpringApplication.run(Tesis20161998Application.class, args);
	}

}
