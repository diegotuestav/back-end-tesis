package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.SponsorshipRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StrategicPlanRequestDTO;
import com.tesis20161998.tesis20161998.services.SponsorshipService;
import com.tesis20161998.tesis20161998.services.StrategicPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sponsorship")
@CrossOrigin(origins = "*")
public class SponsorshipController {

    @Autowired
    private SponsorshipService sponsorshipService;

    @PostMapping("/create/")
    public ResponseEntity<Object> SponsorshipCreate(@RequestBody SponsorshipRequestDTO sponsorshipRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(sponsorshipService.createSponsorship(sponsorshipRequestDTO));
    }

    @PostMapping("/update/{idSponsorship}")
    public ResponseEntity<Object> SponsorshipUpdate(@PathVariable("idSponsorship") Long idSponsorship, @RequestBody SponsorshipRequestDTO sponsorshipRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(sponsorshipService.updateSponsorship(idSponsorship, sponsorshipRequestDTO));
    }

    @GetMapping("/list/{idStrategicPlan}")
    public ResponseEntity<Object> SponsorshipList(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(sponsorshipService.listSponsorships(idStrategicPlan));
    }
}
