package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.StrategicPlanAddUserRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StrategicPlanRequestDTO;
import com.tesis20161998.tesis20161998.services.StrategicPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/strategic-plan")
@CrossOrigin(origins = "*")
public class StrategicPlanController {
    @Autowired
    private StrategicPlanService strategicPlanService;

    @PostMapping("/create/")
    public ResponseEntity<Object> StrategicPlanCreate(@RequestBody StrategicPlanRequestDTO strategicPlanRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(strategicPlanService.createStrategicPlan(strategicPlanRequestDTO));
    }

    @PostMapping("/add-user/")
    public ResponseEntity<Object> StrategicPlanAddUser(@RequestBody StrategicPlanAddUserRequestDTO strategicPlanRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(strategicPlanService.addUsertoStrategicPlan(strategicPlanRequestDTO));
    }

    @GetMapping("/get/{idStrategicPlan}")
    public ResponseEntity<Object> MacroActivityGet(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(strategicPlanService.getStrategicPlan(idStrategicPlan));
    }

    @GetMapping("/list/{idUser}")
    public ResponseEntity<Object> StrategicPlanList(@PathVariable("idUser") Long idUser) {
        return ResponseEntity.status(HttpStatus.OK).body(strategicPlanService.listStrategicPlans(idUser));
    }

    @GetMapping("/list-users/{idStrategicPlan}")
    public ResponseEntity<Object> UserList(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(strategicPlanService.listUsers(idStrategicPlan));
    }

}
