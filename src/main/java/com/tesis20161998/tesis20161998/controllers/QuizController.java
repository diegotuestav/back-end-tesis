package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.QuizRequestDTO;
import com.tesis20161998.tesis20161998.services.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/quiz")
@CrossOrigin(origins = "*")
public class QuizController {
    @Autowired
    private QuizService quizService;

    @PostMapping("/update/")
    public ResponseEntity<Object> QuizUpdate(@RequestBody QuizRequestDTO quizDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(quizService.updateQuiz(quizDTO));
    }

    @GetMapping("/get/{idIndicator}")
    public ResponseEntity<Object> QuizGet(@PathVariable("idIndicator") Long idIndicator) {
        return ResponseEntity.status(HttpStatus.OK).body(quizService.getQuiz(idIndicator));
    }
}
