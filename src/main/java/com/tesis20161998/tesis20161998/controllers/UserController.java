package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/get-login/")
    public ResponseEntity<Object> UserGetLogin(@RequestParam(value = "email") String email, @RequestParam(value = "password") String password) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserLogin(email,password));
    }

}
