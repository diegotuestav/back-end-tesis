package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.SponsorshipRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StakeholderRequestDTO;
import com.tesis20161998.tesis20161998.services.SponsorshipService;
import com.tesis20161998.tesis20161998.services.StakeholderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stakeholder")
@CrossOrigin(origins = "*")
public class StakeholderController {
    @Autowired
    private StakeholderService stakeholderService;

    @PostMapping("/create/")
    public ResponseEntity<Object> StakeholderCreate(@RequestBody StakeholderRequestDTO stakeholderRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(stakeholderService.createStakeholder(stakeholderRequestDTO));
    }

    @GetMapping("/list/{idStrategicPlan}")
    public ResponseEntity<Object> StakeholderList(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(stakeholderService.listStakeholders(idStrategicPlan));
    }
}
