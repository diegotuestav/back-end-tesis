package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.IndicatorRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.QuizResolvedRequestoDTO;
import com.tesis20161998.tesis20161998.dto.response.IndicatorResponseDTO;
import com.tesis20161998.tesis20161998.models.Indicator;
import com.tesis20161998.tesis20161998.services.IndicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/indicator")
@CrossOrigin(origins = "*")
public class IndicatorController {
    @Autowired
    private IndicatorService indicatorService;

    @PostMapping("/update/{idIndicator}")
    public ResponseEntity<Object> IndicatorUpdate(@PathVariable("idIndicator") Long idIndicator, @RequestBody IndicatorRequestDTO indicatorDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(indicatorService.updateIndicator(idIndicator, indicatorDTO));
    }

    @GetMapping("/list/{idStrategicPlan}")
    public ResponseEntity<Object> IndicatorList(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(indicatorService.listIndicators(idStrategicPlan));
    }

    @PostMapping("/take-results")
    public ResponseEntity<Object> IndicatorGetResult(@RequestBody QuizResolvedRequestoDTO quizResolvedDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(indicatorService.takeResults(quizResolvedDTO));
    }
}
