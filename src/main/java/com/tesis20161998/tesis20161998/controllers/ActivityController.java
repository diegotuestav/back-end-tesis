package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.ActivityRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.ActivityUpdateRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StrategicPlanRequestDTO;
import com.tesis20161998.tesis20161998.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/activity")
@CrossOrigin(origins = "*")
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    @PostMapping("/create/")
    public ResponseEntity<Object> ActivityCreate(@RequestBody ActivityRequestDTO activityRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(activityService.createActivity(activityRequestDTO));
    }

    @PostMapping("/update/")
    public ResponseEntity<Object> ActivityUpdate(@RequestBody ActivityUpdateRequestDTO activityRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(activityService.updateActivity(activityRequestDTO));
    }

    @GetMapping("/get/{idActivity}")
    public ResponseEntity<Object> ActivityGet(@PathVariable("idActivity") Long idActivity) {
        return ResponseEntity.status(HttpStatus.OK).body(activityService.getActivity(idActivity));
    }
}
