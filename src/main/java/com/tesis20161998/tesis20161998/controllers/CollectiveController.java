package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.CollectiveRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.CollectiveUpdateRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.SponsorshipRequestDTO;
import com.tesis20161998.tesis20161998.services.CollectiveService;
import com.tesis20161998.tesis20161998.services.SponsorshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/collective")
@CrossOrigin(origins = "*")
public class CollectiveController {
    @Autowired
    private CollectiveService collectiveService;

    @PostMapping("/create/")
    public ResponseEntity<Object> CollectiveCreate(@RequestBody CollectiveRequestDTO collectiveRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(collectiveService.createCollective(collectiveRequestDTO));
    }

    @PostMapping("/update/")
    public ResponseEntity<Object> CollectiveUpdate(@RequestBody CollectiveUpdateRequestDTO collectiveRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(collectiveService.updateCollective(collectiveRequestDTO));
    }

    @GetMapping("/list/{idStrategicPlan}")
    public ResponseEntity<Object> CollectiveList(@PathVariable("idStrategicPlan") Long idStrategicPlan) {
        return ResponseEntity.status(HttpStatus.OK).body(collectiveService.listCollectives(idStrategicPlan));
    }
}
