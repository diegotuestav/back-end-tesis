package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.config.BucketName;
import com.tesis20161998.tesis20161998.models.Document;
import com.tesis20161998.tesis20161998.repositories.DocumentRepository;
import com.tesis20161998.tesis20161998.services.DocumentService;
import com.tesis20161998.tesis20161998.services.FileStore;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static org.apache.http.entity.ContentType.*;
import static org.apache.http.entity.ContentType.IMAGE_JPEG;

@Service
@AllArgsConstructor
public class DocumentServiceImpl implements DocumentService {
    private  final FileStore fileStore;
    private   final DocumentRepository repository;

    @Override
    public Document saveDocument(String title, String description, MultipartFile file) {
        //check if the file is empty
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }

        if (!Arrays.asList(IMAGE_PNG.getMimeType(),
                IMAGE_BMP.getMimeType(),
                IMAGE_GIF.getMimeType(),
                IMAGE_JPEG.getMimeType()).contains(file.getContentType())) {
            throw new IllegalStateException("FIle uploaded is not an image");
        }

        //get file metadata
        Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-Type", file.getContentType());
        metadata.put("Content-Length", String.valueOf(file.getSize()));
        //Save Image in S3 and then save Todo in the database
        String path = String.format("%s/%s", BucketName.TODO_IMAGE.getBucketName(), UUID.randomUUID());
        String fileName = String.format("%s", file.getOriginalFilename());
        try {
            fileStore.upload(path, fileName, Optional.of(metadata), file.getInputStream());
        } catch (IOException e) {
            throw new IllegalStateException("Failed to upload file", e);
        }
        Document document = Document.builder()
                .description(description)
                .title(title)
                .imagePath(path)
                .imageFileName(fileName)
                .build();
        repository.save(document);
        return repository.findByTitle(document.getTitle());
    }

    @Override
    public byte[] downloadDocumentImage(Long id) {
        Document todo = repository.findById(id).get();
        return fileStore.download(todo.getImagePath(), todo.getImageFileName());
    }

    @Override
    public List<Document> getAllDocuments() {
        List<Document> todos = new ArrayList<>();
        repository.findAll().forEach(todos::add);
        return todos;
    }
}
