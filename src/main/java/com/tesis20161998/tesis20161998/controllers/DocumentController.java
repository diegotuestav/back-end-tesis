package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.models.Document;
import com.tesis20161998.tesis20161998.services.DocumentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/document")
@AllArgsConstructor
@CrossOrigin("*")
public class DocumentController {
    DocumentService service;

    @GetMapping
    public ResponseEntity<List<Document>> getTodos() {
        return new ResponseEntity<>(service.getAllDocuments(), HttpStatus.OK);
    }

    @PostMapping(
            path = "/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Document> saveTodo(@RequestParam("title") String title,
                                         @RequestParam("description") String description,
                                         @RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(service.saveDocument(title, description, file), HttpStatus.OK);
    }

    @GetMapping(value = "{id}/image/download")
    public byte[] downloadTodoImage(@PathVariable("id") Long id) {
        return service.downloadDocumentImage(id);
    }
}
