package com.tesis20161998.tesis20161998.controllers;

import com.tesis20161998.tesis20161998.dto.request.IndicatorRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.MacroActivityRequestDTO;
import com.tesis20161998.tesis20161998.services.MacroActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/macroactivity")
@CrossOrigin(origins = "*")
public class MacroActivityController {
    @Autowired
    private MacroActivityService macroActivityService;

    @PostMapping(value = "/update/",consumes = MediaType.ALL_VALUE)
    public ResponseEntity<Object> MacroActivityUpdate(@RequestBody MacroActivityRequestDTO macroActivityDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(macroActivityService.updateMacroActivity(macroActivityDTO));
    }

    @GetMapping("/get/")
    public ResponseEntity<Object> MacroActivityGet(@RequestParam(value = "idStrategicPlan") Long idStrategicPlan,
                                                   @RequestParam(value = "idMacroActivityType") Long idMacroActivityType) {
        return ResponseEntity.status(HttpStatus.OK).body(macroActivityService.getMacroActivity(idStrategicPlan,idMacroActivityType));
    }

    @GetMapping("/get/{idMacroActivity}")
    public ResponseEntity<Object> MacroActivityGetById(@PathVariable("idMacroActivity") Long idMacroActivity) {
        return ResponseEntity.status(HttpStatus.OK).body(macroActivityService.getMacroActivityById(idMacroActivity));
    }
}
