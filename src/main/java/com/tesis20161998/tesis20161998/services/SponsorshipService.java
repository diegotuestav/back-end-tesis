package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.SponsorshipRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.SponsorshipResponseDTO;
import com.tesis20161998.tesis20161998.models.Role;
import com.tesis20161998.tesis20161998.models.Sponsorship;
import com.tesis20161998.tesis20161998.models.StrategicPlan;
import com.tesis20161998.tesis20161998.repositories.RoleRepository;
import com.tesis20161998.tesis20161998.repositories.SponsorshipRepository;
import com.tesis20161998.tesis20161998.repositories.StrategicPlanRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class SponsorshipService {
    @Autowired
    private SponsorshipRepository sponsorshipRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private StrategicPlanRepository strategicPlanRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Transactional
    public SponsorshipResponseDTO createSponsorship(SponsorshipRequestDTO sponsorshipRequestDTO) {
        Sponsorship sponsorship = new Sponsorship();
        Role role = roleRepository.findById(sponsorshipRequestDTO.getIdRole()).get();
        StrategicPlan strategicPlan = strategicPlanRepository.findById(sponsorshipRequestDTO.getIdStrategicPlan()).get();
        sponsorship.setNames(sponsorshipRequestDTO.getNames());
        sponsorship.setLastnames(sponsorshipRequestDTO.getLastnames());
        sponsorship.setEmail(sponsorshipRequestDTO.getEmail());
        sponsorship.setCharge(sponsorshipRequestDTO.getCharge());
        sponsorship.setPhone(sponsorshipRequestDTO.getPhone());
        sponsorship.setRole(role);
        sponsorship.setStrategicPlan(strategicPlan);
        sponsorship = sponsorshipRepository.save(sponsorship);

        SponsorshipResponseDTO sponsorshipResponseDTO = new SponsorshipResponseDTO();

        mapper.map(sponsorship,sponsorshipResponseDTO);

        return sponsorshipResponseDTO;
    }

    @Transactional
    public SponsorshipResponseDTO updateSponsorship(Long idSponsorship, SponsorshipRequestDTO sponsorshipRequestDTO) {
        Sponsorship sponsorship = sponsorshipRepository.findById(idSponsorship).get();
        Role role = roleRepository.findById(sponsorshipRequestDTO.getIdRole()).get();
        sponsorship.setNames(sponsorshipRequestDTO.getNames());
        sponsorship.setLastnames(sponsorshipRequestDTO.getLastnames());
        sponsorship.setEmail(sponsorshipRequestDTO.getEmail());
        sponsorship.setPhone(sponsorshipRequestDTO.getPhone());
        sponsorship.setCharge(sponsorshipRequestDTO.getCharge());
        sponsorship.setRole(role);
        sponsorship = sponsorshipRepository.save(sponsorship);

        SponsorshipResponseDTO sponsorshipResponseDTO = new SponsorshipResponseDTO();

        mapper.map(sponsorship,sponsorshipResponseDTO);

        return sponsorshipResponseDTO;
    }

    public List<SponsorshipResponseDTO> listSponsorships(Long idStrategicPlan){
        List<Sponsorship> sponsorships = sponsorshipRepository.findAllByStrategicPlan_IdStrategicPlan(idStrategicPlan);
        List<SponsorshipResponseDTO> sponsorshipsResponseDTO = new ArrayList<>();
        for(Sponsorship s: sponsorships){
            SponsorshipResponseDTO sponsorshipResponseDTO = new SponsorshipResponseDTO();
            mapper.map(s,sponsorshipResponseDTO);
            sponsorshipsResponseDTO.add(sponsorshipResponseDTO);
        }
        return sponsorshipsResponseDTO;
    }
}
