package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.CollectiveRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.CollectiveUpdateRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.CollectiveResponseDTO;
import com.tesis20161998.tesis20161998.models.*;
import com.tesis20161998.tesis20161998.repositories.CollectiveRepository;
import com.tesis20161998.tesis20161998.repositories.StrategicPlanRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CollectiveService {
    @Autowired
    private CollectiveRepository collectiveRepository;

    @Autowired
    private StrategicPlanRepository strategicPlanRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Transactional
    public CollectiveResponseDTO createCollective(CollectiveRequestDTO collectiveRequestDTO) {
        Collective collective = new Collective();
        StrategicPlan strategicPlan = strategicPlanRepository.findById(collectiveRequestDTO.getIdStrategicPlan()).get();
        collective.setCollective(collectiveRequestDTO.getCollective());
        collective.setStrategicPlan(strategicPlan);
        collective = collectiveRepository.save(collective);

        CollectiveResponseDTO collectiveResponseDTO = new CollectiveResponseDTO();

        mapper.map(collective,collectiveResponseDTO);

        return collectiveResponseDTO;
    }

    public CollectiveResponseDTO updateCollective(CollectiveUpdateRequestDTO collectiveRequestDTO) {
        Collective collective = collectiveRepository.findById(collectiveRequestDTO.getIdCollective()).get();
        collective.setCollective(collectiveRequestDTO.getCollective());
        collective = collectiveRepository.save(collective);
        CollectiveResponseDTO collectiveResponseDTO = new CollectiveResponseDTO();
        mapper.map(collective,collectiveResponseDTO);
        return collectiveResponseDTO;
    }

    public List<CollectiveResponseDTO> listCollectives(Long idStrategicPlan){
        List<Collective> collectives = collectiveRepository.findAllByStrategicPlan_IdStrategicPlan(idStrategicPlan);
        List<CollectiveResponseDTO> collectivesResponseDTO = new ArrayList<>();
        for(Collective c: collectives){
            CollectiveResponseDTO collectiveResponseDTO = new CollectiveResponseDTO();
            mapper.map(c,collectiveResponseDTO);
            collectivesResponseDTO.add(collectiveResponseDTO);
        }
        return collectivesResponseDTO;
    }
}
