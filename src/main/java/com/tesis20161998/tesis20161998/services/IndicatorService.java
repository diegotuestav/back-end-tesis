package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.IndicatorRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.QuizResolvedRequestoDTO;
import com.tesis20161998.tesis20161998.dto.response.IndicatorResponseDTO;
import com.tesis20161998.tesis20161998.models.Indicator;
import com.tesis20161998.tesis20161998.models.Quiz;
import com.tesis20161998.tesis20161998.repositories.IndicatorRepository;
import com.tesis20161998.tesis20161998.repositories.QuizRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndicatorService {
    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private DozerBeanMapper mapper;

    public Indicator updateIndicator(Long idIndicator, IndicatorRequestDTO indicatorDTO) {
        Indicator indicator = indicatorRepository.findById(idIndicator).get();
        indicator.setGoal(indicatorDTO.getGoal());
        indicator = indicatorRepository.save(indicator);
        return indicator;
    }


  public List<IndicatorResponseDTO> listIndicators(Long idStrategicPlan) {
        List<Indicator> indicators = indicatorRepository.findAllByStrategicPlan_IdStrategicPlan(idStrategicPlan);
        List<IndicatorResponseDTO> indicatorsDTO = new ArrayList<>();
        for(Indicator i:indicators){
            IndicatorResponseDTO indicatorDTO = new IndicatorResponseDTO();
            mapper.map(i,indicatorDTO);
            indicatorsDTO.add(indicatorDTO);
        }
        return indicatorsDTO;
    }

    public Indicator takeResults(QuizResolvedRequestoDTO quizResolvedDTO) {
        int cantAnswers = quizResolvedDTO.getAnswers().size();
        int cont = 0;
        Double result = 0.0;
        Double preResult = 0.0;
        while (cont < cantAnswers) {
            if(quizResolvedDTO.getAnswers().get(cont).equals("1 Totalmente desacuerdo")) {
                preResult += 1;
            }
            if(quizResolvedDTO.getAnswers().get(cont).equals("2 En desacuerdo")) {
                preResult += 2;
            }
            if(quizResolvedDTO.getAnswers().get(cont).equals("3 Ni de acuerdo ni en desacuerdo")) {
                preResult += 3;
            }
            if(quizResolvedDTO.getAnswers().get(cont).equals("4 De acuerdo")) {
                preResult += 4;
            }
            if(quizResolvedDTO.getAnswers().get(cont).equals("5 Totalmente de acuerdo")) {
                preResult += 5;
            }
            cont++;
        }
        result = preResult/cantAnswers;
        Quiz quiz = quizRepository.findByIndicator_IdIndicator(quizResolvedDTO.getIdIndicator());
        quiz.setNumResolved(quiz.getNumResolved()+1);
        if(result <= 2.5) {
           quiz.setNumPercent(quiz.getNumPercent()+1);
        }
        quiz = quizRepository.save(quiz);
        Indicator indicator = indicatorRepository.findById(quizResolvedDTO.getIdIndicator()).get();
        indicator.setResult((quiz.getNumPercent()/quiz.getNumResolved())*100);
        indicator = indicatorRepository.save(indicator);
        return indicator;
    }
}
