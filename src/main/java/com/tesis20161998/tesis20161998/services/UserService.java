package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.response.StrategicPlanListResponseDTO;
import com.tesis20161998.tesis20161998.dto.response.UserResponseDTO;
import com.tesis20161998.tesis20161998.models.StrategicPlan;
import com.tesis20161998.tesis20161998.models.User;
import com.tesis20161998.tesis20161998.repositories.StrategicPlanRepository;
import com.tesis20161998.tesis20161998.repositories.UserRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StrategicPlanRepository strategicPlanRepository;

    @Autowired
    private DozerBeanMapper mapper;

    public UserResponseDTO getUserLogin(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email,password);
        UserResponseDTO userDTO = new UserResponseDTO();
        if(user == null){
            userDTO.setCode("400");
            userDTO.setMessage("No existe esta ruta");
            return userDTO;
        }
        mapper.map(user, userDTO);
        return userDTO;
    }



    @PostConstruct
    public void init() {
        User user = new User("Diego","Tuesta","992322717","diegotv536@gmail.com","123456789");
        user = userRepository.save(user);
    }
}
