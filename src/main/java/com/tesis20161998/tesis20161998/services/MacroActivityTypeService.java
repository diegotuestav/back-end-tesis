package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.MacroActivityType;
import com.tesis20161998.tesis20161998.repositories.MacroActivityTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

@Service
public class MacroActivityTypeService {
    @Autowired
    private MacroActivityTypeRepository macroActivityTypeRepository;

    @PostConstruct
    public void init() {
        int cantidad = macroActivityTypeRepository.findAll().size();
        if(cantidad >= 12) return;
        List<MacroActivityType> types = new ArrayList<>();

        MacroActivityType type1 = new MacroActivityType("Definir y preparar al patrocinador del proyecto");
        MacroActivityType type2 = new MacroActivityType("Realizar el workshop de alineación y movilización de los líderes");
        MacroActivityType type3 = new MacroActivityType("Definir el propósito y la identidad del proyecto");
        MacroActivityType type4 = new MacroActivityType("Mapear y clasificar a los stakeholders");
        MacroActivityType type5 = new MacroActivityType("Evaluar las características de la cultura organizacional y sus reflejos en el cambio");
        MacroActivityType type6 = new MacroActivityType("Definir roles y responsabilidades del equipo de proyecto");
        MacroActivityType type7 = new MacroActivityType("Adecuar el ambiente físico a las necesidades del proyecto");
        MacroActivityType type8 = new MacroActivityType("Planificar la asignación y desarrollo del equipo del proyecto");
        MacroActivityType type9 = new MacroActivityType("Evaluar la predisposición del clima para los cambios y sus impactos");
        MacroActivityType type10 = new MacroActivityType("Establecer el Plan de Acción de Gestión del Cambio");
        MacroActivityType type11 = new MacroActivityType("Planificar el Kick-off del proyecto");
        MacroActivityType type12 = new MacroActivityType("Elaborar el Plan Estratégico de Gestión del Cambio");

        types.add(type1);
        types.add(type2);
        types.add(type3);
        types.add(type4);
        types.add(type5);
        types.add(type6);
        types.add(type7);
        types.add(type8);
        types.add(type9);
        types.add(type10);
        types.add(type11);
        types.add(type12);
        types = macroActivityTypeRepository.saveAll(types);
    }
}
