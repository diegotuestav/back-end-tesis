package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.MacroActivityRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.MacroActivityResponseDTO;
import com.tesis20161998.tesis20161998.models.MacroActivity;
import com.tesis20161998.tesis20161998.repositories.MacroActivityRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MacroActivityService {
    @Autowired
    private MacroActivityRepository macroActivityRepository;

    @Autowired
    private DozerBeanMapper mapper;

    public MacroActivityResponseDTO getMacroActivity(Long idStrategicPlan, Long idMacroActivityType) {
        MacroActivity macroActivity = macroActivityRepository.findByStrategicPlan_IdStrategicPlanAndMacroActivityType_IdMacroActivityType(idStrategicPlan,idMacroActivityType);
        MacroActivityResponseDTO macroActivityResponseDTO = new MacroActivityResponseDTO();
        mapper.map(macroActivity, macroActivityResponseDTO);
        return macroActivityResponseDTO;
    }

    public MacroActivityResponseDTO getMacroActivityById(Long idMacroActivity) {
        MacroActivity macroActivity = macroActivityRepository.findById(idMacroActivity).get();
        MacroActivityResponseDTO macroActivityResponseDTO = new MacroActivityResponseDTO();
        mapper.map(macroActivity, macroActivityResponseDTO);
        return macroActivityResponseDTO;
    }

    public MacroActivityResponseDTO updateMacroActivity(MacroActivityRequestDTO macroActivityRequestDTO) {
        MacroActivity macroActivity = macroActivityRepository.findById(macroActivityRequestDTO.getIdMacroActivity()).get();
        macroActivity.setBlankSpace1(macroActivityRequestDTO.getBlankSpace1());
        macroActivity.setBlankSpace2(macroActivityRequestDTO.getBlankSpace2());
        macroActivity.setBlankSpace3(macroActivityRequestDTO.getBlankSpace3());
        macroActivity.setBlankSpace4(macroActivityRequestDTO.getBlankSpace4());
        macroActivity.setDocument1(macroActivityRequestDTO.getDocument1());
        macroActivity.setDocument2(macroActivityRequestDTO.getDocument2());
       macroActivity = macroActivityRepository.save(macroActivity);
        MacroActivityResponseDTO macroActivityResponseDTO = new MacroActivityResponseDTO();
        mapper.map(macroActivity, macroActivityResponseDTO);
        return macroActivityResponseDTO;
    }
}
