package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.SponsorshipRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StakeholderRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.SponsorshipResponseDTO;
import com.tesis20161998.tesis20161998.dto.response.StakeholderResponseDTO;
import com.tesis20161998.tesis20161998.models.*;
import com.tesis20161998.tesis20161998.repositories.*;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class StakeholderService {
    @Autowired
    private StakeholderRepository stakeholderRepository;

    @Autowired
    private StrategicPlanRepository strategicPlanRepository;

    @Autowired
    private AdhesionRepository adhesionRepository;

    @Autowired
    private CollectiveRepository collectiveRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Transactional
    public StakeholderResponseDTO createStakeholder(StakeholderRequestDTO stakeholderRequestDTO) {
        Stakeholder stakeholder = new Stakeholder();
        Profile profile = profileRepository.findById(stakeholderRequestDTO.getIdProfile()).get();
        Adhesion adhesion = adhesionRepository.findById(stakeholderRequestDTO.getIdAdhesion()).get();
        Collective collective = collectiveRepository.findById(stakeholderRequestDTO.getIdCollective()).get();
        StrategicPlan strategicPlan = strategicPlanRepository.findById(stakeholderRequestDTO.getIdStrategicPlan()).get();
        stakeholder.setNames(stakeholderRequestDTO.getNames());
        stakeholder.setLastnames(stakeholderRequestDTO.getLastnames());
        stakeholder.setEmail(stakeholderRequestDTO.getEmail());
        stakeholder.setPhone(stakeholderRequestDTO.getPhone());
        stakeholder.setCollective(collective);
        stakeholder.setAdhesion(adhesion);
        stakeholder.setProfile(profile);
        stakeholder.setStrategicPlan(strategicPlan);
        stakeholder = stakeholderRepository.save(stakeholder);

        StakeholderResponseDTO stakeholderResponseDTO = new StakeholderResponseDTO();

        mapper.map(stakeholder,stakeholderResponseDTO);

        return stakeholderResponseDTO;
    }

    public List<StakeholderResponseDTO> listStakeholders(Long idStrategicPlan){
        List<Stakeholder> stakeholders = stakeholderRepository.findAllByStrategicPlan_IdStrategicPlan(idStrategicPlan);
        List<StakeholderResponseDTO> stakeholdersResponseDTO = new ArrayList<>();
        for(Stakeholder s: stakeholders){
            StakeholderResponseDTO stakeholderResponseDTO = new StakeholderResponseDTO();
            mapper.map(s,stakeholderResponseDTO);
            stakeholdersResponseDTO.add(stakeholderResponseDTO);
        }
        return stakeholdersResponseDTO;
    }

}
