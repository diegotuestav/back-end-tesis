package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.ActivityRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.ActivityUpdateRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.ActivityResponseDTO;
import com.tesis20161998.tesis20161998.models.Activity;
import com.tesis20161998.tesis20161998.models.MacroActivity;
import com.tesis20161998.tesis20161998.models.User;
import com.tesis20161998.tesis20161998.repositories.ActivityRepository;
import com.tesis20161998.tesis20161998.repositories.MacroActivityRepository;
import com.tesis20161998.tesis20161998.repositories.UserRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private MacroActivityRepository macroActivityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DozerBeanMapper mapper;

    public ActivityResponseDTO getActivity(Long idActivity) {
        Activity activity = activityRepository.findById(idActivity).get();
        ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();
        mapper.map(activity,activityResponseDTO);
        return activityResponseDTO;
    }

    @Transactional
    public ActivityResponseDTO createActivity(ActivityRequestDTO activityRequestDTO) {
        Activity activity = new Activity();
        MacroActivity macroActivity = macroActivityRepository.findById(activityRequestDTO.getIdMacroActivity()).get();
        activity.setMacroActivity(macroActivity);
        activity.setTitle(activityRequestDTO.getTitle());
        activity.setDescription(activityRequestDTO.getDescription());
        activity.setEndDate(activityRequestDTO.getEndDate());
        activity.setNotes(activityRequestDTO.getNotes());
        activity.setFinished(activityRequestDTO.isFinished());
        /*for(Long idUser: activityRequestDTO.getIdUsers()){
            User user = userRepository.findById(idUser).get();
            activity.getUsers().add(user);
        }*/
        activity = activityRepository.save(activity);
        /*for(Long idUser: activityRequestDTO.getIdUsers()){
            User user = userRepository.findById(idUser).get();
            user.getActivities().add(activity);
            user = userRepository.save(user);
        }*/

        ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();
        mapper.map(activity,activityResponseDTO);

        return activityResponseDTO;
    }

    @Transactional
    public ActivityResponseDTO updateActivity(ActivityUpdateRequestDTO activityRequestDTO) {
        Activity activity = activityRepository.findById(activityRequestDTO.getIdActivity()).get();
        activity.setTitle(activityRequestDTO.getTitle());
        activity.setDescription(activityRequestDTO.getDescription());
        activity.setEndDate(activityRequestDTO.getEndDate());
        activity.setNotes(activityRequestDTO.getNotes());
        activity.setDocument(activityRequestDTO.getDocument());
        activity.setFinished(activityRequestDTO.isFinished());
        /*for(User u: activity.getUsers()){
            u.getActivities().remove(activity);
        }
        activity.getUsers().removeAll(activity.getUsers());*/

        /*for(Long idUser: activityRequestDTO.getIdUsers()){
            User user = userRepository.findById(idUser).get();
            activity.getUsers().add(user);
        }*/
        activity = activityRepository.save(activity);
        /*for(Long idUser: activityRequestDTO.getIdUsers()){
            User user = userRepository.findById(idUser).get();
            user.getActivities().add(activity);
            user = userRepository.save(user);
        }
*/
        ActivityResponseDTO activityResponseDTO = new ActivityResponseDTO();
        mapper.map(activity,activityResponseDTO);

        return activityResponseDTO;
    }
}
