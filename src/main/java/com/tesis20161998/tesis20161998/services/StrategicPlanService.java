package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.StrategicPlanAddUserRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.StrategicPlanRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.UserRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.*;
import com.tesis20161998.tesis20161998.models.*;
import com.tesis20161998.tesis20161998.repositories.*;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.security.sasl.SaslServer;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class StrategicPlanService {
    @Autowired
    private StrategicPlanRepository strategicPlanRepository;

    @Autowired
    private MacroActivityTypeRepository macroActivityTypeRepository;

    @Autowired
    private MacroActivityRepository macroActivityRepository;

    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private IndicatorTypeRepository indicatorTypeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private DozerBeanMapper mapper;

    @Transactional
    public StrategicPlanResponseDTO createStrategicPlan(StrategicPlanRequestDTO strategicPlanRequestDTO) {
        StrategicPlan strategicPlan = new StrategicPlan();
        User user = userRepository.findById(strategicPlanRequestDTO.getIdUser()).get();
        user.setIdUser(strategicPlanRequestDTO.getIdUser());
        strategicPlan.setOrganizationName(strategicPlanRequestDTO.getOrganizationName());
        strategicPlan.getUsers().add(user);

        List<MacroActivityType> macroActivityTypes = macroActivityTypeRepository.findAll();
        List<IndicatorType> indicatorsType = indicatorTypeRepository.findAll();

        List<MacroActivity> macroActivities = new ArrayList<>();
        List<MacroActivityNoActivityResponseDTO> macroActivitiesDTO = new ArrayList<>();

        for (MacroActivityType type:macroActivityTypes) {
            MacroActivity macroActivity = new MacroActivity();
            macroActivity.setMacroActivityType(type);
            macroActivity.setStrategicPlan(strategicPlan);
            macroActivities.add(macroActivity);
            macroActivity = macroActivityRepository.save(macroActivity);
            MacroActivityNoActivityResponseDTO macroActivityDTO = new MacroActivityNoActivityResponseDTO();
            mapper.map(macroActivity,macroActivityDTO);
            macroActivitiesDTO.add(macroActivityDTO);
        }

        strategicPlan.setMacroActivities(macroActivities);

        List<Indicator> indicators = new ArrayList<>();

        List<IndicatorResponseDTO> indicatorsDTO = new ArrayList<>();

        for (IndicatorType type: indicatorsType) {
            Indicator indicator = new Indicator();
            Quiz quiz = new Quiz();
            indicator.setIndicatorType(type);
            indicator.setQuiz(quiz);
            indicator.setStrategicPlan(strategicPlan);
            indicators.add(indicator);
            quiz.setIndicator(indicator);
            quiz = quizRepository.save(quiz);
            indicator = indicatorRepository.save(indicator);
            IndicatorResponseDTO indicatorDTO = new IndicatorResponseDTO();
            mapper.map(indicator,indicatorDTO);
            indicatorsDTO.add(indicatorDTO);
        }
        strategicPlan.setIndicators(indicators);
        strategicPlan = strategicPlanRepository.save(strategicPlan);
        user.getStrategicPlans().add(strategicPlan);
        user = userRepository.save(user);

        StrategicPlanResponseDTO strategicPlanDTO = new StrategicPlanResponseDTO();
        strategicPlanDTO.setIdStrategicPlan(strategicPlan.getIdStrategicPlan());
        strategicPlanDTO.setOrganizationName(strategicPlan.getOrganizationName());
        strategicPlanDTO.setMacroActivities(macroActivitiesDTO);

        return strategicPlanDTO;
    }

    public List<StrategicPlanListResponseDTO> listStrategicPlans(Long idUser) {
        User user = userRepository.findById(idUser).get();
        List<StrategicPlanListResponseDTO> strategicPlansDTO = new ArrayList<>();
        for(StrategicPlan s: user.getStrategicPlans()) {
            StrategicPlanListResponseDTO strategicPlanDTO = new StrategicPlanListResponseDTO();
            mapper.map(s,strategicPlanDTO);
            strategicPlansDTO.add(strategicPlanDTO);
        }
        return strategicPlansDTO;
    }

    public StrategicPlanResponseDTO getStrategicPlan(Long idStrategicPlan) {
        StrategicPlan strategicPlan = strategicPlanRepository.findById(idStrategicPlan).get();
        StrategicPlanResponseDTO strategicPlanResponseDTO = new StrategicPlanResponseDTO();
        mapper.map(strategicPlan,strategicPlanResponseDTO);
        return strategicPlanResponseDTO;
    }

    public UserResponseDTO addUsertoStrategicPlan(StrategicPlanAddUserRequestDTO strategicPlanDTO) {
        User user = userRepository.findByEmail(strategicPlanDTO.getEmail());
        StrategicPlan strategicPlan = strategicPlanRepository.findById(strategicPlanDTO.getIdStrategicPlan()).get();

        user.getStrategicPlans().add(strategicPlan);
        strategicPlan.getUsers().add(user);

        user = userRepository.save(user);
        strategicPlan = strategicPlanRepository.save(strategicPlan);

        UserResponseDTO userDTO = new UserResponseDTO();
        mapper.map(user,userDTO);
        return userDTO;
    }

    public List<UserResponseDTO> listUsers(Long idStrategicPlan) {
        StrategicPlan strategicPlan = strategicPlanRepository.findById(idStrategicPlan).get();
        List<UserResponseDTO> usersDTO = new ArrayList<>();
        for(User u: strategicPlan.getUsers()) {
            UserResponseDTO userDTO = new UserResponseDTO();
            mapper.map(u,userDTO);
            usersDTO.add(userDTO);
        }
        return usersDTO;
    }
}
