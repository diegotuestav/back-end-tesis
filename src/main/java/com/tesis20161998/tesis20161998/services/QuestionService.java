package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Question;
import com.tesis20161998.tesis20161998.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;

    public void createQuestion(Question question) {
        questionRepository.save(question);
    }
}
