package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.dto.request.AlternativeRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.QuestionRequestDTO;
import com.tesis20161998.tesis20161998.dto.request.QuizRequestDTO;
import com.tesis20161998.tesis20161998.dto.response.QuizResponseDTO;
import com.tesis20161998.tesis20161998.models.Alternative;
import com.tesis20161998.tesis20161998.models.Indicator;
import com.tesis20161998.tesis20161998.models.Question;
import com.tesis20161998.tesis20161998.models.Quiz;
import com.tesis20161998.tesis20161998.repositories.AlternativeRepository;
import com.tesis20161998.tesis20161998.repositories.IndicatorRepository;
import com.tesis20161998.tesis20161998.repositories.QuestionRepository;
import com.tesis20161998.tesis20161998.repositories.QuizRepository;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {
    @Autowired
    private QuizRepository quizRepository;


    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AlternativeRepository alternativeRepository;

    @Autowired
    private DozerBeanMapper mapper;

    public QuizResponseDTO updateQuiz(QuizRequestDTO quizDTO) {
        Quiz quiz = quizRepository.findByIndicator_IdIndicator(quizDTO.getId_indicator());
        quiz.setTitle(quizDTO.getTitle());
        quiz.setNumResolved(0.0);
        quiz.setNumPercent(0.0);
        quiz.setIntroduction(quizDTO.getIntroduction());
        List<QuestionRequestDTO> questionsDTO = quizDTO.getQuestions();
        for(QuestionRequestDTO q:questionsDTO){
            Question question = new Question();
            mapper.map(q, question);
            question.setQuiz(quiz);
            question = questionRepository.save(question);
            List<AlternativeRequestDTO> alternativesDTO = q.getAlternatives();
            for(AlternativeRequestDTO a:alternativesDTO){
                Alternative alternative = new Alternative();
                mapper.map(a, alternative);
                alternative.setQuestion(question);
                alternative = alternativeRepository.save(alternative);
            }
        }
        quiz = quizRepository.save(quiz);
        QuizResponseDTO quizResponseDTO = new QuizResponseDTO();
        mapper.map(quiz,quizResponseDTO);
        return quizResponseDTO;
    }

    public QuizResponseDTO getQuiz(Long id_indicator) {
        Quiz quiz = quizRepository.findByIndicator_IdIndicator(id_indicator);
        QuizResponseDTO quizDTO = new QuizResponseDTO();
        mapper.map(quiz, quizDTO);
        return quizDTO;
    }

}

