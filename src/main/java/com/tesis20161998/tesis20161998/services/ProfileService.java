package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Profile;
import com.tesis20161998.tesis20161998.models.Role;
import com.tesis20161998.tesis20161998.repositories.ProfileRepository;
import com.tesis20161998.tesis20161998.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileService {
    @Autowired
    private ProfileRepository profileRepository;

    @PostConstruct
    public void init() {
        int cantidad = profileRepository.findAll().size();
        if(cantidad >= 4) return;
        List<Profile> profiles = new ArrayList<>();

        Profile profile1 = new Profile("Decisor");
        Profile profile2 = new Profile("Influenciador Directo");
        Profile profile3 = new Profile("Influenciador Indirecto");
        Profile profile4 = new Profile("Espectador");

        profiles.add(profile1);
        profiles.add(profile2);
        profiles.add(profile3);
        profiles.add(profile4);
        profiles = profileRepository.saveAll(profiles);
    }
}
