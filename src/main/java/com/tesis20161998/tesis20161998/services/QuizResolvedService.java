package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Alternative;
import com.tesis20161998.tesis20161998.models.QuizResolved;
import com.tesis20161998.tesis20161998.repositories.AlternativeRepository;
import com.tesis20161998.tesis20161998.repositories.QuizResolvedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuizResolvedService {
    @Autowired
    private QuizResolvedRepository quizResolvedRepository;

    public void createQuizResolved(QuizResolved quizResolved) {
        quizResolvedRepository.save(quizResolved);
    }
}
