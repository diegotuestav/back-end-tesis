package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Document;
import org.springframework.web.multipart.MultipartFile;


import java.util.*;


public interface DocumentService {
    Document saveDocument(String title, String description, MultipartFile file);

    byte[] downloadDocumentImage(Long id);

    List<Document> getAllDocuments();
}


















