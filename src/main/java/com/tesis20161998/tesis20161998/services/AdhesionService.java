package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Adhesion;
import com.tesis20161998.tesis20161998.models.Role;
import com.tesis20161998.tesis20161998.repositories.AdhesionRepository;
import com.tesis20161998.tesis20161998.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdhesionService {
    @Autowired
    private AdhesionRepository adhesionRepository;

    @PostConstruct
    public void init() {
        int cantidad = adhesionRepository.findAll().size();
        if(cantidad >= 6) return;
        List<Adhesion> adhesions = new ArrayList<>();

        Adhesion adhesion1 = new Adhesion("Vendedor");
        Adhesion adhesion2 = new Adhesion("Soporte");
        Adhesion adhesion3 = new Adhesion("Inestable");
        Adhesion adhesion4 = new Adhesion("Probable Resistente");
        Adhesion adhesion5 = new Adhesion("Saboteador Abierto");
        Adhesion adhesion6 = new Adhesion("Saboteador Oculto");

        adhesions.add(adhesion1);
        adhesions.add(adhesion2);
        adhesions.add(adhesion3);
        adhesions.add(adhesion4);
        adhesions.add(adhesion5);
        adhesions.add(adhesion6);
        adhesions = adhesionRepository.saveAll(adhesions);
    }
}
