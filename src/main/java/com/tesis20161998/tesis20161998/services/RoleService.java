package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Role;
import com.tesis20161998.tesis20161998.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @PostConstruct
    public void init() {
        int cantidad = roleRepository.findAll().size();
        if(cantidad >= 3) return;
        List<Role> roles = new ArrayList<>();

        Role role1 = new Role("Patrocinador");
        Role role2 = new Role("Coordinador de cómite de patrocinio");
        Role role3 = new Role("Miembro de cómite de patrocinio");

        roles.add(role1);
        roles.add(role2);
        roles.add(role3);
        roles = roleRepository.saveAll(roles);
    }
}
