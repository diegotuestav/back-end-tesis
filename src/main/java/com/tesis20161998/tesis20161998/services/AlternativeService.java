package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.Alternative;
import com.tesis20161998.tesis20161998.repositories.AlternativeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlternativeService {
    @Autowired
    private AlternativeRepository alternativeRepository;

    public void createAlternative(Alternative alternative) {
        alternativeRepository.save(alternative);
    }
}
