package com.tesis20161998.tesis20161998.services;

import com.tesis20161998.tesis20161998.models.IndicatorType;
import com.tesis20161998.tesis20161998.repositories.IndicatorTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class IndicatorTypeService {
    @Autowired
    private IndicatorTypeRepository indicatorTypeRepository;

    @PostConstruct
    public void init() {
        int cantidad = indicatorTypeRepository.findAll().size();
        if(cantidad >= 6) return;
        List<IndicatorType> indicators = new ArrayList<>();

        IndicatorType indicator1 = new IndicatorType("Interesados con baja satisfacción", "Conocer el " +
                "porcentaje de interesados con baja satisfacción laboral " +
                "para poder impulsar diversos incentivos para motivar su trabajo dentro del proyecto", "Nibs",
                "Número de interesados con baja satisfacción laboral",
                "Tint", "Total de interesados", "Porcentaje", "(Nibs/Tint)*100" );
        IndicatorType indicator2 = new IndicatorType("Interesados con desconocimiento del objetivo del proyecto",
                "Conocer la cantidad de interesados con desconocimiento del objetivo del proyecto para " +
                        "poder reforzar o reformular los medios de difusión de comunicación interna dentro de la empresa", "Nidob",
                "Número de interesados con desconocimiento del objetivo del proyecto",
                "Tint", "Total de interesados", "Porcentaje", "(Nidob/Tint)*100" );
        IndicatorType indicator3 = new IndicatorType("Tasa de absentismo laboral", "Conocer el porcentaje de " +
                "absentismo laboral injustificado del periodo actual frente al anterior para revisar cuales son las principales " +
                "causas de las faltas de los empleados y abarcar sus problemas de manera más específica", "Mensual","Nac",
                "Número de ausencias no justificadas del período actual", "Nan",
                "Número de ausencias no justificadas del período anterior", "Porcentaje", "((Nac/Nan)-1)*100" );
        IndicatorType indicator4 = new IndicatorType("Tasa de equipos con demoras en entregar objetivos parciales",
                "Conocer la tasa de equipos con retrasos en entregables para analizar las fechas propuestas " +
                        "en conjunto a cada equipo, conociendo sus debilidades y fortalezas para poder mejorar los tiempos",
                "Mensual", "Ner", "Número de equipos con retraso",
                "Teq", "Cantidad total de equipos", "Porcentaje", "(Ner/Teq)*100" );
        IndicatorType indicator5 = new IndicatorType("Renuncias voluntarias", "Conocer la cantidad de renuncias" +
                " voluntarias dentro de un período para mejorar los enfoques del proyecto en lo que los interesados salgan más beneficiados","Mensual",
                "Nrv", "Número de renuncias voluntarias",
                "Cco", "Cantidad de colaboradores", "Porcentaje","(Nrv/Cco)*100" );

        IndicatorType indicator6 = new IndicatorType("Promedio del conocimiento logrado con el entrenamiento",
                "Conocer el puntaje promedio obtenido de las evaluaciones en cada entrenamiento para " +
                        "saber si los empleados retuvieron el conocimiento aprendido para poder aplicarlo dentro del proyecto ",
                "Sne", "Sumatoria de notas de evaluación",
                "Cev", "Cantidad de evaluaciones", "Puntaje","Sne/Cev" );
        indicators.add(indicator1);
        indicators.add(indicator2);
        indicators.add(indicator3);
        indicators.add(indicator4);
        indicators.add(indicator5);
        indicators.add(indicator6);
        indicators = indicatorTypeRepository.saveAll(indicators);
    }
}
