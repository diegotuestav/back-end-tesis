package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

@Data
public class StakeholderRequestDTO {
    private String names;
    private String lastnames;
    private String email;
    private String phone;
    private Long idProfile;
    private Long idAdhesion;
    private Long idCollective;
    private Long idStrategicPlan;
}
