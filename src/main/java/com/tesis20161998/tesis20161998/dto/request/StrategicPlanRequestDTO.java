package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

@Data
public class StrategicPlanRequestDTO {
    private Long idUser;
    private String organizationName;
}
