package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

@Data
public class CollectiveRequestDTO {
    private String collective;
    private Long idStrategicPlan;
}
