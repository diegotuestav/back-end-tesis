package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

@Data
public class SponsorshipRequestDTO {
    private String names;
    private String lastnames;
    private String email;
    private String phone;
    private String charge;
    private Long idRole;
    private Long idStrategicPlan;
}
