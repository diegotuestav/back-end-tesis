package com.tesis20161998.tesis20161998.dto.request;

import com.tesis20161998.tesis20161998.dto.response.ActivityResponseDTO;
import lombok.Data;

import java.sql.Blob;
import java.util.List;

@Data
public class MacroActivityRequestDTO {
    private Long idMacroActivity;
    private String blankSpace1;
    private String blankSpace2;
    private String blankSpace3;
    private String blankSpace4;
    private byte[] document1;
    private byte[] document2;
}
