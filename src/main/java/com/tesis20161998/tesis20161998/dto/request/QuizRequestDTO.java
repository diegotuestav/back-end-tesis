package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class QuizRequestDTO {
    private Long id_indicator;
    private String title;
    private String introduction;
    private List<QuestionRequestDTO> questions;
}
