package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserRequestDTO {
    private Long idUser;
    private String names;
    private String lastnames;
    private String phone;
    private String email;
    private byte[] photo;
}
