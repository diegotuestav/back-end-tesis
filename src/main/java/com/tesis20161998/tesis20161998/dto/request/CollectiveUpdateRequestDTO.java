package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

@Data
public class CollectiveUpdateRequestDTO {
    private Long idCollective;
    private String collective;
}
