package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class QuestionRequestDTO {
    private Integer numQuestion;
    private String statement;
    private Double score;
    private List<AlternativeRequestDTO> alternatives;
}
