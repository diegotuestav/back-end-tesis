package com.tesis20161998.tesis20161998.dto.request;

import lombok.Data;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

@Data
public class ActivityUpdateRequestDTO {
    private Long idActivity;
    private String title;
    private String description;
    private String notes;
    private Date endDate;
    private boolean finished;
    private List<Long> idUsers;
    private byte[] document;
}
