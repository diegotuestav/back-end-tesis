package com.tesis20161998.tesis20161998.dto.request;


import lombok.Data;

import java.util.List;

@Data
public class QuizResolvedRequestoDTO {
    private Long idIndicator;
    private List<String> answers;
}
