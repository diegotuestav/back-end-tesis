package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class IndicatorTypeResponseDTO {
    private Long idIndicatorType;
    private String indicator;
    private String description;
    private String period;
    private String formula;
    private String nameVariable1;
    private String variableDescription1;
    private String nameVariable2;
    private String variableDescription2;
    private String measureUnit;
}
