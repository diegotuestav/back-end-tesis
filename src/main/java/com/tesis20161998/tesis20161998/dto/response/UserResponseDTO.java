package com.tesis20161998.tesis20161998.dto.response;

import com.tesis20161998.tesis20161998.models.StrategicPlan;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserResponseDTO extends BaseResponse{
    private Long idUser;
    private String names;
    private String lastnames;
    private String phone;
    private String email;
    private byte[] photo;
}
