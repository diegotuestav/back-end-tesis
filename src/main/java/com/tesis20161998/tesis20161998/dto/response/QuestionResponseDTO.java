package com.tesis20161998.tesis20161998.dto.response;

import com.tesis20161998.tesis20161998.dto.request.AlternativeRequestDTO;
import lombok.Data;

import java.util.List;

@Data
public class QuestionResponseDTO {
    private Long idQuestion;
    private Integer numQuestion;
    private String statement;
    private Double score;
    private List<AlternativeResponseDTO> alternatives;
}
