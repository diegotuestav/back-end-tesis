package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class StakeholderResponseDTO {
    private Long idStakeholder;
    private String names;
    private String lastnames;
    private String email;
    private String phone;
    private ProfileResponseDTO profile;
    private AdhesionResponseDTO adhesion;
    private CollectiveResponseDTO collective;
}
