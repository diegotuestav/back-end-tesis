package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class MacroActivityNoActivityResponseDTO {
    private Long idMacroActivity;
    private String blankSpace1;
    private String blankSpace2;
    private String blankSpace3;
    private String blankSpace4;
    private byte[] document1;
    private byte[] document2;
}
