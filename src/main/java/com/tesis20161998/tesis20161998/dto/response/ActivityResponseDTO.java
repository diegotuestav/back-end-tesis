package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

@Data
public class ActivityResponseDTO {
    private Long idActivity;
    private String title;
    private String description;
    private String notes;
    private Date endDate;
    private List<UserResponseDTO> users;
    private boolean finished;
    private byte[] document;
}
