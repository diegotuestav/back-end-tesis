package com.tesis20161998.tesis20161998.dto.response;

import com.tesis20161998.tesis20161998.dto.request.QuestionRequestDTO;
import lombok.Data;

import java.util.List;

@Data
public class QuizResponseDTO {
    private Long id_indicator;
    private String title;
    private String introduction;
    private List<QuestionResponseDTO> questions;
}
