package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class MacroActivityResponseDTO {
    private Long idMacroActivity;
    private List<ActivityResponseDTO> activities;
    private String blankSpace1;
    private String blankSpace2;
    private String blankSpace3;
    private String blankSpace4;
}
