package com.tesis20161998.tesis20161998.dto.response;

import com.tesis20161998.tesis20161998.models.*;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
public class StrategicPlanResponseDTO {
    private Long idStrategicPlan;
    private String organizationName;
    private List<MacroActivityNoActivityResponseDTO> macroActivities;
}
