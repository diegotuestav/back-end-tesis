package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class AdhesionResponseDTO {
    private Long idAdhesion;
    private String adhesion;
}
