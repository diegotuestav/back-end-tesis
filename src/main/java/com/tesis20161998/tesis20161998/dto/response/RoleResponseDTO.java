package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class RoleResponseDTO {
    private Long idRole;
    private String roleName;
}
