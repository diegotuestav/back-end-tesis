package com.tesis20161998.tesis20161998.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IndicatorResponseDTO {
    private Long idIndicator;
    private Double goal;
    private Double variable1;
    private Double variable2;
    private Double result;
    private IndicatorTypeResponseDTO indicatorType;
}
