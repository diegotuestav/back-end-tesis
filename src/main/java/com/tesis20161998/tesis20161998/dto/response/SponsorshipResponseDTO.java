package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class SponsorshipResponseDTO {
    private Long idSponsorship;
    private String names;
    private String lastnames;
    private String email;
    private String phone;
    private String charge;
    private RoleResponseDTO role;
}
