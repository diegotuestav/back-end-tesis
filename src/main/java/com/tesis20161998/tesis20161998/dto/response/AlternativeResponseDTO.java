package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;

@Data
public class AlternativeResponseDTO {
    private Long idAlternative;
    private String alternative;
    private boolean isCorrect;
}
