package com.tesis20161998.tesis20161998.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseResponse {
    private String code ="200";
    private String message = "OK";
}
