package com.tesis20161998.tesis20161998.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BucketName {
    TODO_IMAGE("backend-tesis-storage");
    private final String bucketName;
}