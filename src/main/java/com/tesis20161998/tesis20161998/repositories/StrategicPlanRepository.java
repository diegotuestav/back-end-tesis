package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.StrategicPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StrategicPlanRepository extends JpaRepository<StrategicPlan, Long> {
}