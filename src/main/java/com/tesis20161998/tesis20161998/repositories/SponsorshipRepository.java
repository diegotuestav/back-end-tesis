package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Quiz;
import com.tesis20161998.tesis20161998.models.Sponsorship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SponsorshipRepository extends JpaRepository<Sponsorship, Long> {
    List<Sponsorship> findAllByStrategicPlan_IdStrategicPlan(Long idStrategicPlan);
}