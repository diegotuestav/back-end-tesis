package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Alternative;
import com.tesis20161998.tesis20161998.models.Indicator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlternativeRepository extends JpaRepository<Alternative, Long> {
}