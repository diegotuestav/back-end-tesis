package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.MacroActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MacroActivityRepository extends JpaRepository<MacroActivity, Long> {
    MacroActivity findByStrategicPlan_IdStrategicPlanAndMacroActivityType_IdMacroActivityType(Long idStrategicPlan, Long idMacroActivityType);
}