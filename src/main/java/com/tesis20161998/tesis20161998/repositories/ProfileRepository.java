package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
}