package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Document;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<Document, Long> {
    Document findByTitle(String title);
}
