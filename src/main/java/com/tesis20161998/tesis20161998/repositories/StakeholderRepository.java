package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Sponsorship;
import com.tesis20161998.tesis20161998.models.Stakeholder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StakeholderRepository extends JpaRepository<Stakeholder, Long> {
    List<Stakeholder> findAllByStrategicPlan_IdStrategicPlan(Long idStrategicPlan);
}