package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.MacroActivityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MacroActivityTypeRepository extends JpaRepository<MacroActivityType, Long> {
}
