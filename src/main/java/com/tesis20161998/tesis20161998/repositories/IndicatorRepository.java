package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Indicator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndicatorRepository extends JpaRepository <Indicator, Long> {
    List<Indicator> findAllByStrategicPlan_IdStrategicPlan(Long IdStrategicPlan);
}