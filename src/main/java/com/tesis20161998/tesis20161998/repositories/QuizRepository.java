package com.tesis20161998.tesis20161998.repositories;

import com.tesis20161998.tesis20161998.models.Indicator;
import com.tesis20161998.tesis20161998.models.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    Quiz findByIndicator_IdIndicator(Long idIndicator);
}